const path = require('path')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CompressionPlugin = require('compression-webpack-plugin')
const HappyPack = require('happypack')
const happyThreadPool = HappyPack.ThreadPool({ size: 6 })
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const WorkboxPlugin = require('workbox-webpack-plugin');

const productionPlugins = [
  new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      regExp: /\.js$|\.css$|\.html$/,
      threshold: 1,
  }),
  new WorkboxPlugin.GenerateSW({
    // Do not precache images
    exclude: [/\.(?:png|jpg|jpeg|svg)$/],

    // Define runtime caching rules.
    runtimeCaching: [{
      // Match any request that ends with .png, .jpg, .jpeg or .svg.
      urlPattern: /\.(?:png|jpg|jpeg|svg)$/,

      // Apply a cache-first strategy.
      handler: 'networkFirst',

      options: {
        // Use a custom cache name.
        cacheName: 'images',

        // Only cache 10 images.
        expiration: {
          maxEntries: 10,
        },
      },
    }],
  })
]

module.exports = (env, argv) => {
    console.log(`Using mode: [${argv.mode}]`)
    const isProduction = argv.mode === 'production'

    return {
        cache: true,
        devtool: isProduction ? 'cheap-module-source-map' : 'source-map',
        entry: {
            buildHtml: './src/buildHtml.js',
            client: './src/client/client.js'
        },
        output: {
          path: __dirname + '/public',
          filename: '[name].js'
        },
        target: 'node',
        externals: nodeExternals(),
        optimization: {
          minimizer: [
            new UglifyJsPlugin({
              cache: true,
              parallel: true,
              sourceMap: true // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({})
          ]
        },
        plugins: [
            new MiniCssExtractPlugin({
              // Options similar to the same options in webpackOptions.output
              // both options are optional
              filename: "styles.css"
            }),
            new CopyWebpackPlugin([{ context: `${__dirname}/src/assets`, from: `*.*` }]),
            ...(isProduction ? productionPlugins : []),
            // turn on for bundle size analytics
            // new BundleAnalyzerPlugin({
            //     analyzerMode: 'static',
            // }),
        ],

        module: {
          rules: [
            {
              test: /\.js$/,
              use: [{
                  loader: 'babel-loader',
                  options: {
                      plugins: ['@babel/plugin-proposal-class-properties'],
                      presets: [
                          ['@babel/preset-env', {
                              targets: {
                                  browsers: ['last 2 versions'],
                              },
                          }],
                          ['@babel/react'],
                      ],
                  },
              }],
            }, {
                test: /\.(woff|woff2|eot|ttf)$/,
                use: ['url-loader'],
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: ['file-loader'],
            }, {
                test: /\.css$/,
                use: [{
                    loader: MiniCssExtractPlugin.loader,
                  }, {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1,
                        modules: true,
                        localIdentName: '[name]__[local]___[hash:base64:5]',
                    },
                }, {
                    loader: 'postcss-loader',
                    options: {
                        ident: 'postcss',
                    },
                }]
            },
          ],
        },
        resolve: {
            extensions: ['.js'],
        },
    }
}
