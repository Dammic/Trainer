import '../../node_modules/lightgallery.js/dist/js/lightgallery.min.js';
// import { tns } from '../../node_modules/tiny-slider/src/tiny-slider';

const scrollToSection = (sectionName) => {
  setTimeout(() =>{
    document.querySelector(`#${sectionName}`).scrollIntoView({ 
      behavior: 'smooth',
      block: "start",
      inline: "start"
    });
  }, 0);
}

const scrollTriggerElements = document.querySelectorAll('[data-scroll-to]');

scrollTriggerElements.forEach((element) => {
  element.addEventListener('click', scrollToSection.bind(null, element.dataset.scrollTo));
});

document.querySelector('#back-to-top').addEventListener('click', scrollToSection.bind(null, 'app-container'));


// add gallery section
lightGallery(document.getElementById('lightgallery'));

// const slider = tns({
//   container: '#reviews-carousel',
//   items: 1,
//   slideBy: 1,
//   autoplay: true,
//   speed: 800,
//   loop: false,
//   rewind: true,
//   axis: 'vertical',
//   controls: false,
//   autoplayButtonOutput: false,
//   autoHeight: true,
// });
