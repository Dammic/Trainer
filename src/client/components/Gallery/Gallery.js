import React from 'react';
import classNames from 'classnames';
import { LabeledSlope } from '../LabeledSlope';
import image1 from './images/image1.jpg';
import image2 from './images/image2.jpg';
import image3 from './images/image3.jpg';
import image1Thumb from './images/image1-thumb.jpg';
import image2Thumb from './images/image2-thumb.jpg';
import image3Thumb from './images/image3-thumb.jpg';

const images = [
  {
    imageSrc: image1,
    imageThumb: image1Thumb,
    vertical: true,
  }, {
    imageSrc: image2,
    imageThumb: image2Thumb,
    vertical: true,
  }, {
    imageSrc: image3,
    imageThumb: image3Thumb,
    vertical: true,
  }
];

import styles from './Gallery.css';

const Gallery = () => (
  <div className={styles.gallery} id="gallery">
    <LabeledSlope label="Zobacz mnie w pracy" />
    <div className={styles.galleryContent}>
      <input className={styles.input} type="checkbox" id="more-toggle" />

      <div className={styles.content} id="lightgallery">
        {images.map((val, index) => (
          <a key={index} href={val.imageSrc} aria-label={`gallery image ${index}`}>
            <img src={val.imageThumb} alt={`gallery image ${index}`} className={classNames(styles.thumb, { [styles.vertical]: val.vertical })}/>
          </a>
        ))}
      </div>
      {images.length > 4 && (
        <React.Fragment>
          <label className={styles.showMore} htmlFor="more-toggle">[ Pokaż Więcej ]</label>
          <label className={classNames(styles.showMore, styles.showLess)} htmlFor="more-toggle">[ Pokaż Mniej ]</label>
        </React.Fragment>
      )}
    </div>
  </div>
);

export default Gallery;
