import React from 'react';
import classNames from 'classnames';
import { parseHtmlTags } from '../../utils';

import styles from './Description.css';

const Description = ({ description, heading, className }) => (
  <div className={classNames(styles.textContainer, className)}>
    <h3 className={styles.heading}>
      {heading} 
    </h3>
    <p className={styles.description}>
      {parseHtmlTags(description)}
    </p>
  </div>
);

export default Description;
