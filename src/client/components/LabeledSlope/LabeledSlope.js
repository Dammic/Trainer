import React from 'react';

import styles from './LabeledSlope.css';

const LabeledSlope = ({ label }) => (
  <div className={styles.labeledSlope}>
    <h2 className={styles.label}>
      {label}
      <i className={styles.downArrow} />
    </h2>
    <div className={styles.slope} />
  </div>
);

export default LabeledSlope;
