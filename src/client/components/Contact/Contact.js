import React from 'react';
import classNames from 'classnames';
import { LabeledSlope } from '../LabeledSlope';
import homeImage from './images/home.png';
import mailImage from './images/mail.png';
import phoneImage from './images/phone.png';
import mapImage from './images/map.jpg';

import styles from './Contact.css';

const Contact = () => (
  <div className={styles.contact} id="contact">
    <LabeledSlope label="Niech jutro stanie sie już dziś - zarezerwuj konsultacje z trenerem personalnym" />
    <div className={styles.content}>
      <div>
        <h3 className={styles.trainerName}>
          Trener Personalny<br/>
          Mateusz Korcala
        </h3>
        <div className={styles.contactInfo}>
          <a className={classNames(styles.infoRow, styles.clickableRow)} href="tel:+48882004001">
            <img src={phoneImage} alt="phone icon" className={styles.icon} />
            <h3 className={styles.rowTextContent}>882-004-001</h3>
            <i className={styles.rightArrow} />
          </a> 
          <a className={classNames(styles.infoRow, styles.clickableRow)} href="mailto:korcala007@gmail.com">
            <img src={mailImage} alt="mail icon" className={classNames(styles.icon, styles.mailIcon)} />
            <h3 className={styles.rowTextContent}>korcala007@gmail.com</h3>
            <i className={styles.rightArrow} />
          </a> 
          <div className={styles.infoRow}>
            <img src={homeImage} alt="home icon" className={classNames(styles.icon, styles.homeIcon)} />
            <h3 className={styles.rowTextContent}>Kraków</h3>
          </div>
        </div>
        <h2 className={styles.placeInfo}>
          Prowadzę treningi na siłowni:<br/>
          <a
            href="https://fitnessplatinium.pl/bratyslawska/"
            rel="nofollow noopener noreferrer"
            target="_blank"
            className={classNames(styles.infoRow, styles.gymInfo, styles.clickableRow)}
          >
            <h3 className={styles.rowTextContent}>
              Fitness Platinum Bratysławska<br/>
              ul. Bratysławska 4 31-201 Kraków
            </h3>
            <i className={styles.rightArrow} />
          </a>
        </h2>
        <span className={styles.englishOpportunity}>
          Możliwość treningu w języku angielskim!<br />
          (English trainings available!)
        </span>
      </div>
    
      <a
        className={classNames(styles.clickableRow, styles.map)}
        rel="nofollow noopener noreferrer"
        target="_blank"
        href="https://www.google.pl/maps/place/Fitness+Platinium%C2%AE+Bratys%C5%82awska/@50.0837623,19.9338344,17z/data=!3m1!4b1!4m5!3m4!1s0x47165a566d539ef5:0xa991f7eab97f825a!8m2!3d50.0837623!4d19.9360231"
      >
        <img src={mapImage} alt="map image of gym" className={styles.mapImage}/> 
      </a>
    </div>
  </div>
);

export default Contact;
