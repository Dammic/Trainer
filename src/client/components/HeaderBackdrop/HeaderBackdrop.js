import React from 'react';
import backdropImage from './images/backdropImage.jpg'; 

import styles from './HeaderBackdrop.css';

const HeaderBackdrop = () => (
  <div className={styles.headerBackdropContainer}>
    <div className={styles.headerBackdrop}>
      <div className={styles.backdroupContent}>
        <div className={styles.helpButton} data-scroll-to="about">
          Jak mogę Ci pomóc?
        </div>
      </div>
      <div className={styles.welcomePhoto} />
    </div>
    <div className={styles.underHeaderCurve} />
  </div>
);

export default HeaderBackdrop;
