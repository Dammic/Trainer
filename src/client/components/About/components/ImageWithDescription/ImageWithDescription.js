import React, { Fragment } from 'react';
import classNames from 'classnames';
import { parseHtmlTags } from '../../../../utils';
import { Description } from '../../../Description';

import styles from './ImageWithDescription.css';

const ImageWithDescription = ({ image, imageAlt, heading, description, isReverted, vertical }) => (
  <div className={classNames(styles.imageWithDescription, { [styles.reverted]: isReverted })}>
    <img className={classNames(styles.image, { [styles.vertical]: vertical })} src={image} alt={imageAlt} />
    <Description heading={heading} description={description} />
  </div>
);

export default ImageWithDescription;
