import React from 'react';
import { LabeledSlope } from '../LabeledSlope';
import { ImageWithDescription } from './components/ImageWithDescription';
import descriptions from './descriptions';

import styles from './About.css';

const About = () => (
  <div className={styles.about} id="about">
    <LabeledSlope label="Mateusz Korcala - Trener personalny z Krakowa" />
    <div className={styles.descriptions}>
      <ImageWithDescription
        image={descriptions[1].image}
        imageAlt={descriptions[1].imageAlt}
        heading={descriptions[1].heading}
        description={descriptions[1].description}
        vertical
      />
      <ImageWithDescription
        image={descriptions[2].image}
        imageAlt={descriptions[2].imageAlt}
        heading={descriptions[2].heading}
        description={descriptions[2].description}
        isReverted
      />
    </div>
  </div>
);

export default About;
