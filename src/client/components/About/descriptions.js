import file1 from './images/file1.jpg';
import file2 from './images/file2.jpg';

const descriptions = {
  1: {
    image: file1,
    imageAlt: 'Strong man pulling weight from the ground',
    heading: 'Jestem dyplomowanym trenerem personalnym. Posiadam szkolenia z treningu funkcjonalnego, Kettlebell, TRX, a także mobilności ruchowej.',
    description: 'Każdy ma swój cel - moim jest sprawić, by moi podopieczni osiągneli własne cele i ambicje w jak najbardziej wygodny i przyjazny dla nich sposob. Wieloletnie doświadczenie w treningach pozwolilo mi posiąść wiedzę, jak optymalnie osiągnąć zróznicowane grupy celów i tym doświadczeniem dzielę sie ze swoimi podopiecznymi - niezależnie czy mówimy o budowie masy miesniowej, zrzuceniu zbędnej tkanki tluszczowej, czy tez poprawie sylwetki i postury ciała.\n\nDla swoich podopiecznych przygotowuję indywidualne plany treningowe i żywieniowe, by mogli oni jak najefektywniej poprawić swoją sylwetkę, bez marnowania czasu na metody, które nie dzialają. Efektem naszej pracy będzie lepsze zdrowie, kondycja, a także zwiększona pewność siebie i wiele innych pozytywnych przemian, które na pewno odczujesz.'
  },
  2: {
    image: file2,
    imageAlt: 'Focused man training his biceps with barbell',
    heading: 'Prowadzę treningi personalne dla kobiet i mężczyzn, dopasowane indywidualnie pod zamierzony cel.',
    description: 'Przed rozpoczęciem współpracy przeprowadzam wstępny wywiad aby jak najlepiej poznać Twoje oczekiwania oraz ewentualne dolegliwości. W zależności od podjętej formy współpracy układam indywidualny plan treningowy oraz dietetyczny aby zmaksymalizować efekty ćwiczeń.\n'
  }
};

export default descriptions;
