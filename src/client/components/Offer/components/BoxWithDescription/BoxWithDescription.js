import React from 'react';
import classNames from 'classnames';
import { Description } from '../../../Description';

import styles from './BoxWithDescription.css';

const BoxWithDescription = ({ description, heading, isReverted, children }) => (
  <div className={classNames(styles.container, { [styles.reverted]: isReverted })}>
    <div className={styles.nameWrapper}>
      <div className={styles.nameBox}>
        {children} 
      </div>
    </div>
    <div className={styles.arrowUp} />
    <div className={styles.descriptionContainer}>
      <div className={styles.arrowLeft} />
      <Description heading={heading} description={description} className={styles.descriptionBox} />
      <div className={styles.arrowRight} />
    </div>
  </div>
);

export default BoxWithDescription;
