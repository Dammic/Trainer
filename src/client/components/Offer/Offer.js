import React from 'react';
import classNames from 'classnames';
import { LabeledSlope } from '../LabeledSlope';
import { BoxWithDescription } from './components/BoxWithDescription';
import descriptions from './descriptions';

import runningImage from './images/running.png';
import plateImage from './images/plate.png';
import dumbellImage from './images/dumbell.png';
import messageImage from './images/message.png';

import styles from './Offer.css';

const renderFirstBox = () => (
  <div className={styles.firstBox}>
    <img src={dumbellImage} alt="dumbell" className={styles.dumbellIcon} />
    <h2 className={styles.boxDescription}>Treningi personalne</h2>
  </div>  
);

const renderSecondBox = () => (
  <div className={styles.secondBox}>
    <img src={runningImage} alt="running man" className={styles.runningIcon} />
    <h2 className={styles.boxDescription}>Plany<br/>treningowe</h2>
  </div>  
);

const renderThirdBox = () => (
  <div className={styles.thirdBox}>
    <img src={plateImage} alt="plate with food" className={styles.plateIcon} />
    <h2 className={classNames(styles.boxDescription, styles.diet)}>Plany<br/>żywieniowe</h2>
  </div>  
);

const renderFourthBox = () => (
  <div className={styles.fourthBox}>
    <img src={messageImage} alt="message icon" className={styles.messageIcon} />
    <h2 className={classNames(styles.boxDescription, styles.message)}>Konsultacje<br/>online</h2>
  </div>
);

const Offer = () => (
  <div className={styles.offer} id="offer">
    <LabeledSlope label="Oferta" />
    <div className={styles.content}>
      <BoxWithDescription description={descriptions[1].description} heading={descriptions[1].heading}>
        {renderFirstBox()}
      </BoxWithDescription>
      <BoxWithDescription description={descriptions[2].description} heading={descriptions[2].heading} isReverted>
        {renderSecondBox()}
      </BoxWithDescription>
      <BoxWithDescription description={descriptions[3].description} heading={descriptions[3].heading}>
        {renderThirdBox()}
      </BoxWithDescription>
      <BoxWithDescription description={descriptions[4].description} heading={descriptions[4].heading} isReverted>
        {renderFourthBox()}
      </BoxWithDescription>
    </div>
  </div>
);

export default Offer;
