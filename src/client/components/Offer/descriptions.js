const descriptions = {
  1: {
    description: 'Na każdym treningu będę czuwał, aby ćwiczenia były wykonywane poprawnie i przede wszystkim bezpiecznie. Podejmując współpracę ze mną, możesz być pewny, że zmotywuję Cię do wysiłku, jakiego sam prawdopodobnie byś nie podjął – co zdecydowanie przyspieszy osiągnięcie sukcesu.',
    heading: 'Profesjonalne Treningi personalne pod okiem doświadczonego trenera'
  },
  2: {
    description: 'Dopasowane pod konkrentne potrzeby Treningi personalne gwarantują najlepsze efekty. Omówimy wszystkie ćwiczenia podczas wspólnego treningu. Odpowiednie nakierowanie zapewni Ci komfort i poczucie bezpieczeństwa podczas wykonywanych ćwiczeń. Każdy plan jest indywidualnie periodyzowany aby zapewnić jak najbardziej efektywny wpływ treningu siłowego na twoją sylwetkę.',
    heading: 'Indywidualnie dopasowane programy ćwiczeń'
  },
  3: {
    description: 'Nie jestem zwolennikiem jałowego jedzenia - zainspiruje Cię zdrową kuchnią oraz pysznymi propozycjami dań. Suplementacja, korekta, wsparcie - na bieżąco monitorujemy postępy.',
    heading: 'Dieta ułożona do Twoich potrzeb i wymagań'
  },
  4: {
    description: 'Trenować i prowadzić dietę można nie tylko na siłowni. Współpracę możemy nawiązać droga internetową. Plan dietetyczny i Plan treningowy będzie tak samo efektywny jak plan nabyty w kontakcie be to be .',
    heading: 'Jestem do dyspozycji także online'
  }
};

export default descriptions;
