import React from 'react';
import { LabeledSlope } from '../LabeledSlope';

import styles from './Reviews.css';

const reviews = [
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fdamian.michalski.18659%2Fposts%2F148046993172001", 284],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D2658699704236831%26id%3D100002904828409", 316],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpaulina.mastaj%2Fposts%2F10215815690600888", 161],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fgrzegorz.polkowski.5%2Fposts%2F2510982229021955", 256],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fmateusz.kawonczyk%2Fposts%2F2635857299843342", 294],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fkonrad.bebak%2Fposts%2F10217613480307859", 218],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fsura.c.12%2Fposts%2F2371377772923951", 319],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fbartosz.straczek%2Fposts%2F1923969031043254", 218],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fpermalink.php%3Fstory_fbid%3D1794591100663097%26id%3D100003366729191", 199],
  ["https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fdariusz.piwowarczyk.7%2Fposts%2F2000347756699582", 180],
]
const MIN_HEIGHT = Math.max.apply(null, reviews.map((val) => val[1]));
const REVIEW_WIDTH = 700;

const Reviews = () => (
  <div className={styles.reviews}>
    <LabeledSlope label="Recenzje na mój temat" />
    <div className={styles.textContainer}>
      <p className={styles.text}>Prowadziłem i wciąż prowadzę wielu podopiecznych w różnym wieku, z którymi udało się osiagnac zamierzone cele.<br/><br/>Oto opinie ludzi, którzy mieli ze mną do czynienia:</p>
    </div>
    <div className={styles.reviewsContainer}>
      <div className={styles.content}>
        <div id="reviews-carousel">
          {reviews.map(([url, height], index) => (
            <div key={url}>
              <iframe
                title={`review-${index}`}
                src={`${url}&width=auto`}
                style={{ border: 'none', overflow: 'hidden' }} scrolling="no" frameBorder="0" allow="encrypted-media"
              />
            </div>
          ))}
        </div>
        <a
          className={styles.link}
          href="https://www.facebook.com/trenerkorcala/"
          target="_blank" 
          rel="nofollow noopener noreferrer"
        >
          Zobacz wszystkie
        </a>
      </div>
    </div>
  </div>
);

export default Reviews;
