import React from 'react';
import mountainImage from './images/mountain.svg';

import styles from './Logo.css';

const Header = () => (
  <div className={styles.logo}>
    <h1 className={styles.logoText}>
      Trener Personalny<br/>
      Mateusz Korcala
    </h1>
    <div className={styles.logoImage} /> 
  </div>
);

export default Header;
