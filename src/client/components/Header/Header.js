import React from 'react';
import Logo from './components/Logo';

import styles from './Header.css';

const Header = () => (
  <React.Fragment>
    <input className={styles.hamburgerInput} type="checkbox" id="hamburger-toggle" />
    <div className={styles.header}>
      <Logo />

      <div className={styles.navigation}>
        <div className={styles.navigationMenu}>
          <label htmlFor="hamburger-toggle" data-scroll-to="about">
            <div className={styles.item}>O mnie</div>
          </label>
          <label htmlFor="hamburger-toggle" data-scroll-to="offer">
            <div className={styles.item}>Oferta</div>
          </label>
          <label htmlFor="hamburger-toggle" data-scroll-to="contact">
            <div className={styles.item}>Kontakt</div>
          </label>
          <label htmlFor="hamburger-toggle" data-scroll-to="gallery">
            <div className={styles.item}>Galeria</div>
          </label>
        </div>

        <div className={styles.navigationDesktop}>
          <div className={styles.item} data-scroll-to="about">O mnie</div>
          <div className={styles.item} data-scroll-to="offer">Oferta</div>
          <div className={styles.item} data-scroll-to="contact">Kontakt</div>
          <div className={styles.item} data-scroll-to="gallery">Galeria</div>
        </div>
        <div className={styles.navigationMobile}>
          <label className={styles.hamburgerLabel} htmlFor="hamburger-toggle">
            <div className={styles.hamburgerImage} />
          </label>
        </div>
      </div>
    </div>
  </React.Fragment>
);

export default Header;
