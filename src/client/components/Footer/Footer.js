import React from 'react';

import styles from './Footer.css';

const Footer = () => (
  <div className={styles.footer}>
    <div className={styles.copyrightInfo}>
      <span className={styles.copyrightText}>© 2018 Mateusz Korcala</span>
    </div>
    <div className={styles.footerNavigation}>
      <div>
        <div className={styles.navItem}><span data-scroll-to="about">O mnie</span></div>
        <div className={styles.navItem}><span data-scroll-to="offer">Oferta</span></div>
        <div className={styles.navItem}><span data-scroll-to="contact">Kontakt</span></div>
        <div className={styles.navItem}><span data-scroll-to="gallery">Galeria</span></div>
      </div>
      <div className={styles.backToTop} id="back-to-top">
        <div className={styles.arrowUp} />
      </div>
    </div>
  </div>
);

export default Footer;
