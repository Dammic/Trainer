import React from 'react';

import Header from './components/Header';
import HeaderBackdrop from './components/HeaderBackdrop';
import { About } from './components/About';
import { Offer } from './components/Offer';
// import { Reviews } from './components/Reviews';
import { Contact } from './components/Contact';
import { Gallery } from './components/Gallery';
import { Footer } from './components/Footer';

import styles from './App.css';

const App = () => (
  <div className={styles.appContainer} id="app-container">
    <div className={styles.main}>
      <Header />
      <HeaderBackdrop />
      <About />
      <Offer />
      <Contact />
      <Gallery />
      <Footer />
    </div>
  </div>
);

export default App;
