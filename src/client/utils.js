import React, { Fragment } from 'react';

export const parseHtmlTags = (text) => {
  return text.split('\n').map((item, key) => {
    return <Fragment key={key}>{item}<br/></Fragment>
  });
};
