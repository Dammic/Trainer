const title = '💪 Trener Personalny - Kraków Bratysławska - Mateusz Korcala';

const description = 'Profesjonalne Treningi Personalne w Fitness Platinum Bratysławska z trenerem Mateuszem Korcalą - zrzucanie wagi, budowa masy mięsniowej, poprawa sylwetki i kondycji';

const Html = ({ body }) => `
  <!DOCTYPE html>
  <html lang="pl-pl" dir="ltr">
    <head>
      <title>${title}</title>
      <meta name="description" content="${description}">
      <link rel="canonical" href="https://trenerkorcala.pl" />
      <meta name="google-site-verification" content="cZ5qEAqv6cHKcDNECm_JFHYdB9-Ptw46V5Y0M_tEzKY" />

      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
      <link rel="manifest" href="/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">

      <link rel="prefetch" href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.11/css/lightgallery.min.css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css" rel="stylesheet" >
      <link href="styles.css" rel="stylesheet">
    </head>
    <body style="margin:0">
      <div id="app">${body}</div>
      <script type="application/ld+json">
      {
        "@context": "http://schema.org/",
        "@type": "Service",
        "additionalType": "http://www.productontology.org/doc/Personal_trainer",
        "category": {
          "@type": "PhysicalActivityCategory",
          "name": "Treningi personalne",
          "description": "Zrzucanie wagi, budowa masy miesniowej, poprawa sylwetki i kondycji"
        },
        "name": "Trener personalny",
        "areaServed": {
          "@type": "GeoShape",
          "address": {
            "@type": "PostalAddress",
            "addressCountry": "PL",
            "postalCode": "31201",
            "streetAddress": "Bratysławska 4"
          }
        },
        "hoursAvailable": [{
          "@type": "OpeningHoursSpecification",
          "dayOfWeek": [
            "Tuesday",
            "Monday",
            "Wednesday",
            "Thursday",
            "Friday"
	  ],
          "closes": "23:00:00",
          "opens": "10:00:00",
          "name": "Dni pracujace"
        }, {
          "@type": "OpeningHoursSpecification",
          "dayOfWeek": [
            "Saturday"
	  ],
          "closes": "20:00:00",
          "opens": "10:00:00",
          "name": "Weekend"
        }],
        "offers": [{
          "@type": "offer",
          "name": "Trening personalny",
          "category": {
            "@type": "PhysicalActivityCategory",
            "name": "Treningi personalne",
            "description": "Zrzucanie wagi, budowa masy miesniowej, poprawa sylwetki i kondycji"
          },
          "areaServed": {
            "@type": "GeoShape",
            "address": {
              "@type": "PostalAddress",
              "addressCountry": "PL",
              "postalCode": "31201",
              "streetAddress": "Bratysławska 4"
            }
          }
        }],
        "providerMobility": "static",
        "serviceType": "Treningi personalne i konsultacje",
        "description": "Treningi personalne z doswiadczonym trenerem personalnym z Fitness Platinum Bratyslawska"
      }
      </script>
      <script>
        // Check that service workers are supported
        if ('serviceWorker' in navigator) {
          // Use the window load event to keep the page load performant
          window.addEventListener('load', () => {
            navigator.serviceWorker.register('/service-worker.js');
          });
        }
      </script>
      <script src="./client.js" type="text/javascript"></script>
    </body>
  </html>
`;

export default Html;
