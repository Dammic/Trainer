import fs from 'fs';
import React from 'react';
import { renderToString } from 'react-dom/server';
import App from './client/App';
import Html from './client/Html';

const body = renderToString(<App />);
const html = Html({ body })
fs.writeFile('public/index.html', html, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("HTML file has been saved!");
}); 
